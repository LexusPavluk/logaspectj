package runners;

import aspectj.Loggable;

public class AspectRunner {
    public static void main(String[] args) {
        AspectRunner runner = new AspectRunner();
        String string  = "Let's log it!!!";
        int stringLength = runner.getStringLength(string);
        runner.printString(string);
        runner.printString("String length: " + stringLength);
        runner.printString("String length: ", stringLength);
        runner.printString(null);
    }

    @Loggable
    public int getStringLength(String str) {
        return str.length();
    }

    @Loggable
    public void printString(String str) {
        System.out.println(str);
    }

    @Loggable
    public void printString(String str, int size) {
        printString(str + size);
    }

}
