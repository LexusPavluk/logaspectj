package aspectj;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;

@Slf4j
public class CatchingInProceed {

    public static Object proceed(ProceedingJoinPoint joinPoint) {
        Object obj = null;
        try {
            obj = joinPoint.proceed();
        } catch (Throwable e) {
            log.warn("Aspect proceeding JoinPoint error: ", e);
        }
        return obj;
    }

}
