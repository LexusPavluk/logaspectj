package aspectj.impl;

import aspectj.CatchingInProceed;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Aspect
public class LogMethodsAspect {
    private static final String METHOD = "Method : ";
    private static final String ARGUMENTS = ", args: ";
    private static final String RESULT = "======== result = ";
    private static final String EQUALLY = " = '";
    private static final String QUOTE = "'";

    @Around("@annotation(aspectj.Loggable) && execution(public !void * (..))")
    public Object logSignatureAndResult(ProceedingJoinPoint joinPoint) {
        logBefore(joinPoint);

        Object result = CatchingInProceed.proceed(joinPoint);
        log.info(RESULT + (Objects.isNull(result) ? null : result.toString()));
        return result;
    }

    @Before("@annotation(aspectj.Loggable) && execution(public void * (..))")
    public void logVoidMethods(JoinPoint joinPoint) {
        logBefore(joinPoint);
    }


    private void logBefore(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();

        log.info(METHOD + signature +
                ARGUMENTS + Arrays.stream(joinPoint.getArgs())
                .map(obj -> Objects.isNull(obj) ? null :
                        obj.getClass().getSimpleName() + EQUALLY + obj + QUOTE)
                .collect(Collectors.joining(", "))
        );
    }
}
